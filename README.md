# Lectures-Talks-Articles-Papers
For quick referencing and flexing on casuals.

<hr/>

## Podcasts
___Sean Carroll's Mindscape Podcast___<br>
&nbsp; [104 | David Rosen and Scott Miles on the Neuroscience of Music and Creativity](https://www.preposterousuniverse.com/podcast/2020/07/06/104-david-rosen-and-scott-miles-on-the-neuroscience-of-music-and-creativity/)<br>
&nbsp; [82 | Robin Carhart-Harris on Psychedelics and the Brain](https://www.preposterousuniverse.com/podcast/2020/02/03/82-robin-carhart-harris-on-psychedelics-and-the-brain/)<br>
&nbsp; [78 | Daniel Dennett on Minds, Patterns, and the Scientific Image](https://www.preposterousuniverse.com/podcast/2020/01/06/78-daniel-dennett-on-minds-patterns-and-the-scientific-image/)<br>
&nbsp; [Episode 1: Carol Tavris on Mistakes, Justification, and Cognitive Dissonance](https://www.preposterousuniverse.com/podcast/2018/07/09/episode-1-carol-tavris-on-mistakes-justification-and-cognitive-dissonance/)<br>
___Random___<br>
&nbsp; [Joe Rogan Experience #1513 - Andrew Huberman](https://www.youtube.com/watch?v=gLJowTOkZVo) [Neuroscience]<br>
***

## Miscellaneous
___Articles___<br>
&nbsp; [Paul Dirac: The Mozart of Science](https://www.ias.edu/ideas/2008/farmelo-on-dirac)<br>
&nbsp; [Richer command line interfaces](http://ballingt.com/rich-terminal-applications-2/)<br>
&nbsp; [Digging into RISC-V and how I learn new things](https://blog.jessfraz.com/post/digging-into-risc-v-and-how-i-learn-new-things/)<br>
&nbsp; [Beating C with 80 lines of Haskell: wc](https://chrispenner.ca/posts/wc)<br>
&nbsp; [The Siren Song of Little Languages](http://www.wilfred.me.uk/blog/2019/03/24/the-siren-song-of-little-languages/)<br>
___Books___<br>
&nbsp; [Peak: Secrets from the New Science of Expertise](https://volante.se/bocker/peak/)<br>
&nbsp; [Make It Stick: The Science of Successful Learning](https://www.hup.harvard.edu/catalog.php?isbn=9780674729018)<br>
&nbsp; [Cognitive Neuroscience: A Very Short Introduction](https://www.veryshortintroductions.com/view/10.1093/actrade/9780198786221.001.0001/actrade-9780198786221)<br>
___Tech Talks___<br>
&nbsp; [Demo of Xlambda V1](https://www.youtube.com/watch?v=PyXzosc0iVc)<br>
&nbsp; [Console Hacking 2016 - PS4: PC Master Race [33c3] - YouTube](https://www.youtube.com/watch?v=6hxEUm-pHUE) [Youtube black holed]<br>
&nbsp; [Console Hacking 2013: WiiU [30c3] - YouTube](https://www.youtube.com/watch?v=hZRz0xikaAU) [Youtube black holed]<br>
&nbsp; [Project Management Using Kanban by Jeremy Ricketts](https://www.youtube.com/watch?v=qFMJziAlBeM)<br>
&nbsp; [[ HN Kansai #52 ] Genius Russian Programmers - Myth or Reality ?](https://www.youtube.com/watch?v=8Gwe-KWPp44)<br>
&nbsp; [Ted Nelson on What Modern Programmers Can Learn From the Past](https://www.youtube.com/watch?v=edZgkNoLdAM)<br>
***

#### Learning, Memory, & Productivity ####
___Articles___<br>
&nbsp; [2011 : WHAT SCIENTIFIC CONCEPT WOULD IMPROVE EVERYBODY'S COGNITIVE TOOLKIT?](https://www.edge.org/response-detail/11783)<br>
___Talks___<br>
&nbsp; [The 50 Minute Focus Finder - Dean Jackson - I Love Marketing](https://www.youtube.com/watch?v=5onClAZJgdY)<br>
&nbsp; [How to Learn Anything... Fast - Josh Kaufman](https://www.youtube.com/watch?v=EtJy69cEOtQ)<br>
&nbsp; [How to become a memory master | Idriz Zogaj | TEDxGoteborg](https://www.youtube.com/watch?v=9ebJlcZMx3c)<br>
&nbsp; [Memory techniques - Your secret weapon in the information age | Simon Reinhard | TEDxTUM](https://www.youtube.com/watch?v=KCLdryjDl6E)<br>
&nbsp; [How to use memory techniques to improve education | Boris Nikolai Konrad | TEDxDenHelder](https://www.youtube.com/watch?v=_qIBe0h0-Ig)<br>
&nbsp; [2017 Personality and Its Transformations (University of Toronto)](https://www.youtube.com/watch?v=kYYJlNbV1OM&list=PL22J3VaeABQApSdW8X71Ihe34eKN6XhCi) [Lecture Series]<br>
&nbsp; [How to learn any language in six months | Chris Lonsdale | TEDxLingnanUniversity](https://www.youtube.com/watch?v=d0yGdNEWdn0)<br>
***

### Graphics & Gamedev ###
___Articles___<br>
&nbsp; [How to make your game run at 60fps](https://medium.com/@tglaiel/how-to-make-your-game-run-at-60fps-24c61210fe75)<br>
&nbsp; [Handles are the better pointers](https://floooh.github.io/2018/06/17/handles-vs-pointers.html)<br>
___Tech Talks___<br>
&nbsp; [Game Development with SDL 2.0 (Steam Dev Days 2014)](https://www.youtube.com/watch?v=MeMPCSqQ-34)<br>
&nbsp; [Level Up Your Game: The Untapped Potential of Roguelikes](https://www.youtube.com/watch?v=IE19Te46kYc)<br>
&nbsp; [How to Make and Self-Publish a Game in 12 Months](https://www.youtube.com/watch?v=4tbPbMRcMFQ)<br>
&nbsp; [Tech Toolbox for Game Programmers](https://www.youtube.com/watch?v=W_okgL6HJX8)<br>
&nbsp; [Doom: A Classic Game Post-Mortem](https://www.youtube.com/watch?v=NnkCujnYNSo&feature=youtu.be)<br>
&nbsp; [Tarn Adams - Villains in Dwarf Fortress](https://www.youtube.com/watch?v=4-7TtPX5uhg)<br>
&nbsp; [Dwarf Fortress, Moon Hunters, and Practices in Procedural Generation](https://www.youtube.com/watch?v=v8zwPdPvN10&feature=youtu.be&t=555)<br>
&nbsp; [Dwarf Fortress Design Inspirations - Zach and Tarn Adams](https://www.youtube.com/watch?v=ZMRsScwdPcE)<br>
&nbsp; [Synchronizing Game Components - Robert Virding](https://www.youtube.com/watch?v=eHTePEtLV-c)<br>
&nbsp; [PRACTICE 2016: Tarn Adams on Vimeo](https://vimeo.com/215929684) [black holed]<br>
***

### General CompSci, Theory, & Language Design ###
___Articles___<br>
&nbsp; [The actor model in 10 minutes](https://www.brianstorti.com/the-actor-model/)<br>
&nbsp; [Category Theory in 10 Minutes](https://www.slideshare.net/JordanParmer/category-theory-in-10-minutes-77309719) [Slides]<br>
___Tech Talks___<br>
&nbsp; [YOW! Lambda Jam 2019 - Ken Scambler - Applied Category Theory](https://www.youtube.com/watch?v=iwvl0tBJhoM)<br>
&nbsp; ["The Mess We're In" by Joe Armstrong](https://www.youtube.com/watch?v=lKXe3HUG2l4)<br>
&nbsp; [Concurrency + Distribution = Scalability + Availability (...) - Francesco Cesarini](https://www.youtube.com/watch?v=g9GG_vtSy7w)<br>
&nbsp; [Story of Commodore from the Computer Engineers' Perspective](https://www.youtube.com/watch?v=cwr8tTFGZtI)<br>
&nbsp; [Types, and Why You Should Care](https://www.youtube.com/watch?v=0arFPIQatCU)<br>
&nbsp; [Robert Virding - On Language Design (Lambda Days 2016)](https://www.youtube.com/watch?v=f3rP3JRq7Mw)<br>
&nbsp; [Joe Armstrong - Keynote: The Forgotten Ideas in Computer Science - Code BEAM SF 2018](https://www.youtube.com/watch?v=-I_jE0l7sYQ)<br>
&nbsp; [Beyond Lists: High Performance Data Structures - Phil Trelford](https://www.youtube.com/watch?v=hx2vOwbB-X0)<br>
&nbsp; [Growing a Language, by Guy Steele](https://www.youtube.com/watch?v=_ahvzDzKdB0)<br>
&nbsp; [Systems Programming Panel - Andrei Alexandrescu, Walter Bright, Scott Meyers | DConf2017 - YouTube](https://www.youtube.com/watch?v=Lo6Q2vB9AAg) [Youtube black holed]<br>
&nbsp; [GOTO 2017 • Programming Across Paradigms • Anjana Vakil](https://www.youtube.com/watch?v=Pg3UeB-5FdA)<br>
&nbsp; [GOTO 2017 • Idée Fixe • David Nolen](https://www.youtube.com/watch?v=lzXHMy4ewtM)<br>
&nbsp; [How things are, and how they could be by Fred Hebert](https://www.youtube.com/watch?v=Z28SDd9bXcE)<br>
&nbsp; [Leslie Lamport: Thinking Above the Code](https://www.youtube.com/watch?v=-4Yp3j_jk8Q)<br>
&nbsp; [RustConf 2017 - Closing Keynote: Safe Systems Software and the Future of Computing by Joe Duffy](https://www.youtube.com/watch?v=EVm938gMWl0)<br>
&nbsp; [Sebastian Blessing - Run, actor, run](https://www.youtube.com/watch?v=tCa7WpHtkgM)<br>
&nbsp; [Tudor Gîrba - Moldable development](https://www.youtube.com/watch?v=Pot9GnHFOVU)<br>
&nbsp; [GOTO 2012 • Mythbusting Remote Procedure Calls • Steve Vinoski](https://www.youtube.com/watch?v=so6fNXLFixg)<br>
&nbsp; [What We Talk About When We Talk About Distributed Systems - Alvaro Videla](https://www.youtube.com/watch?v=wcnqnYwrJWg)<br>
&nbsp; [Category Theory for the Working Hacker by Philip Wadler](https://www.youtube.com/watch?v=V10hzjgoklA)<br>
&nbsp; [How to design co-programs](https://patternsinfp.wordpress.com/2018/11/21/how-to-design-co-programs/)<br>
&nbsp; [Top-down vs Bottom-up Programming](https://two-wrongs.com/top-down-vs-bottom-up-programming.html)<br>
&nbsp; [Let's stop copying C](https://eev.ee/blog/2016/12/01/lets-stop-copying-c/)<br>
&nbsp; [Murphy vs Satan: Why programming secure systems is still hard - Rod Chapman - Bristech 2016](https://www.youtube.com/watch?v=ruuZVFbRlr4)<br>
&nbsp; [Kevlin Henney - Procedural Programming: It's Back? It Never Went Away](https://www.youtube.com/watch?v=otAcmD6XEEE)<br>
&nbsp; [Avishai Ish-Shalom - Resilient Design 101: Queue Theory](https://www.youtube.com/watch?v=zsozsY0GPFQ)<br>
&nbsp; [How we program multicores - Joe Armstrong](https://www.youtube.com/watch?v=bo5WL5IQAd0)<br>
&nbsp; [Gerry Sussman - We Really Don't Know How to Compute!](https://www.youtube.com/watch?v=O3tVctB_VSU)<br>
&nbsp; [ISSCC2018 - 50 Years of Computer Architecture:From Mainframe CPUs to Neural-Network TPUs](https://www.youtube.com/watch?v=NZS2TtWcutc)<br>
&nbsp; [Scott Lystig Fritchie - Wide World of Actors - Code BEAM SF 2018](https://www.youtube.com/watch?v=uv-3ptTD8hg)<br>
&nbsp; [Bret Victor The Future of Programming](https://www.youtube.com/watch?v=8pTEmbeENF4)<br>
***

### Retro Computing & Demoscene ###
___Articles___<br>
&nbsp; [Interview with Japanese demoscener - 0x4015 ](http://6octaves.blogspot.com/2017/06/interview-with-japanese-demoscener.html?m=1)<br>
___Tech Talks___<br>
&nbsp; [The Ultimate AMIGA 500 Talk ( AHS EPI-006 )](https://www.youtube.com/watch?v=oF-1ynRqNUo)<br>
&nbsp; [8 Bit & '8 Bitish' Graphics-Outside the Box](https://www.youtube.com/watch?v=aMcJ1Jvtef0)<br>
&nbsp; [The Ultimate Amiga 500 Talk [32c3] - YouTube](https://www.youtube.com/watch?v=BbVAvDbzXFk) [Youtube black holed]<br>
&nbsp; [PRGE 2017 - The 8-Bit Guy David Murray - Portland Retro Gaming Expo 1080p](https://www.youtube.com/watch?v=nEDTFhmf5Sc)<br>
***

### Linux, Emacs & Ops ###
___Articles___<br>
&nbsp; [So you want to build an embedded Linux system?](https://jaycarlson.net/embedded-linux/)<br>
___Tech Talks___<br>
&nbsp; ['Become a GDB Power User' - Greg Law [ ACCU 2016 ]](https://www.youtube.com/watch?time_continue=2&v=713ay4bZUrw)<br>
&nbsp; [EmacsConf 2019 - 14 - Magit deep dive - Jonathan Chu](https://www.youtube.com/watch?v=vS7YNdl64gY&feature=youtu.be&t=632)<br>
&nbsp; [Evil Mode: Or, How I Learned to Stop Worrying and Love Emacs](https://www.youtube.com/watch?v=JWD1Fpdd4Pc)<br>
&nbsp; [Give me 15 minutes and I'll change your view of Linux tracing](https://www.youtube.com/watch?v=GsMs3n8CB6g)<br>
***

### OS Design & General OS Stuff###
___Tech Talks___<br>
&nbsp; ["Runtime scheduling: theory and reality" by Eben Freeman](https://www.youtube.com/watch?v=8g9fG7cApbc)<br>
&nbsp; ["Look ma, no OS! Unikernels and their applications" by Matt Bajor](https://www.youtube.com/watch?v=W9F4pn9Lngc)<br>
&nbsp; ["When the OS gets in the way" by Mark Price](https://www.youtube.com/watch?v=-6nrhSdu--s)<br>
&nbsp; [Operating System Scheduler Design for Multicore Architectures](https://www.youtube.com/watch?v=Rl_NtL3vYZM)<br>
&nbsp; [The Design of a Reliable and Secure Operating System by Andrew Tanenbaum](https://www.youtube.com/watch?v=oS4UWgHtRDw)<br>
&nbsp; [An Introduction to Schedulers - Kushal Pisavadia](https://www.youtube.com/watch?v=ADid256OiB8)<br>
&nbsp; [The Multikernel: A new OS Architecture for Scalable Multicore Systems](https://www.youtube.com/watch?v=xSKDv_ycwHo)<br>
&nbsp; [The HIPPEROS RTOS: A Song of Research and Development](https://fosdem.org/2020/schedule/event/uk_hipperos/)<br>
&nbsp; [A roadmap for the Hurd?](https://archive.fosdem.org/2019/schedule/event/roadmap_for_the_hurd/)<br>
&nbsp; [FreeRTOS on RISC-V: Running the FreeRTOS kernel in RISC-V emulators and RISC-V hardware](https://archive.fosdem.org/2019/schedule/event/riscvfreertos/)<br>
&nbsp; [Embedded FreeBSD on a five-core RISC-V processor using LLVM How hard can it be?](https://www.youtube.com/watch?v=c6_g5KMUX-E)<br>
&nbsp; [From L3 to seL4 what have we learnt in 20 years of L4 microkernels?](https://www.youtube.com/watch?v=RdoaFc5-1Rk)<br>
&nbsp; [Multitasking on Cortex-M class MCUs A deep-dive into the Chromium-EC OS](https://www.youtube.com/watch?v=yHqaspeGJRw)<br>
***

### Digital Electronics, VHDL & FPGAs ###
___Articles___<br>
&nbsp; [The History, Status, and Future of FPGAs](https://queue.acm.org/detail.cfm?id=3411759) [Article]<br>
___Tech Talks___<br>
&nbsp; [Adam Taylor FPGA Kongress Preso 2017](https://www.youtube.com/watch?v=WGOZbXSxuNI)<br>
&nbsp; [Lesson learned from Retro-uC and search for ideal HDL for open source silicon](https://www.youtube.com/watch?v=g1lzjQAvC1I)<br>
***

### Drawing ###
&nbsp; [Iterative Drawing - The Fastest Way to Improve](https://www.youtube.com/watch?v=k0ufz75UvHs) [Video]<br>
&nbsp; [Thinking with a Pencil ](https://www.amazon.com/Thinking-Pencil-Henning-Nelms/dp/1626541841) [Book]<br>
&nbsp; [Drawabox](https://drawabox.com/) [Tutorial series]<br>
***

### General Functional Progamming ###
___Tech Talks___<br>
&nbsp; [PolyConf 16 / OOP to FP / Julia Gao](https://www.youtube.com/watch?v=R_TQtthAGQc)<br>
&nbsp; [PolyConf 16: Oden - A Functional Programming Language for the Go Ecosystem / Oskar Wickstrom](https://www.youtube.com/watch?v=qRm_58RA9ns)<br>
&nbsp; [Lies My OO Teacher Told Me](https://www.youtube.com/watch?v=cdAVWYw-dO0)<br>
&nbsp; ["Functional Data Structures" by Tessa Kelly](https://www.youtube.com/watch?v=UBuFHCsz16A)<br>
&nbsp; [On the paradigm of functional programming - Stanislaw Ambroszkiewicz (Lambda Days 2017)](https://www.youtube.com/watch?v=Hb2zwlpMK_U)<br>
&nbsp; [Erlang Factory SF 2016 - Keynote - John Hughes - Why Functional Programming Matters](https://www.youtube.com/watch?v=Z35Tt87pIpg)<br>
&nbsp; [Rob Martin - Teaching functional programming to noobs (Lambda Days 2016)](https://www.youtube.com/watch?v=bmFKEewRRQg)<br>
***

### Compilers ###
___Articles___<br>
&nbsp; [A Brief And Brisk Overview of Compiler Architecture](https://felixangell.com/blogs/compilers-brief-and-brisk)<br>
___Tech Talks___<br>
&nbsp; [2018 LLVM Developers’ Meeting: C. Schafmeister “Lessons Learned Implementing Common Lisp with LLVM”](https://www.youtube.com/watch?v=mbdXeRBbgDM)<br>
***

### Writing & Documentation ###
___Articles___<br>
&nbsp; [It's time to start writing](https://alexnixon.github.io/2019/12/10/writing.html)<br>
&nbsp; [Writing, Briefly](http://www.paulgraham.com/writing44.html)<br>
&nbsp; [Write Like You Talk](http://www.paulgraham.com/talk.html)<br>
&nbsp; [The Age of the Essay](http://www.paulgraham.com/essay.html)<br>
___Talks___<br>
&nbsp; [What nobody tells you about documentation](https://www.youtube.com/watch?v=t4vKPhjcMZg)/[The documentation system](https://documentation.divio.com/)<br>
&nbsp; [7 Ways to Hack Your Brain to Write Fluently by Dan Allen](https://www.youtube.com/watch?v=r6RXRi5pBXg)<br>
&nbsp; [LEADERSHIP LAB: The Craft of Writing Effectively](https://www.youtube.com/watch?v=vtIzMaLkCaM)<br>
***

## Programming Languages ##

### Go ###
___Articles___<br>
&nbsp; [Why Go? – Key advantages you may have overlooked](https://yourbasic.org/golang/advantages-over-java-python/)<br>
___Tech Talks___<br>
&nbsp; [The State of Go What's new in Go 1.10](https://www.youtube.com/watch?v=iR7LPAXWfmw)<br>
***

### Ada & SPARK ###
___Articles___<br>
&nbsp; [Proving Memory Operations - A SPARK Journey](https://blog.adacore.com/proving-memory-operations-a-spark-journey)<br>
___Books___<br>
&nbsp; [Building High Integrity Applications with SPARK](https://www.cambridge.org/core/books/building-high-integrity-applications-with-spark/F213D9867D2E271F5FF3EDA765D48E95)<br>
&nbsp; [Analysable Real-Time Systems: Programmed in Ada](https://www.amazon.co.uk/Analysable-Real-Time-Systems-Programmed-Ada/dp/1530265509/)<br>
___Tech Talks___<br>
&nbsp; [Securing the Future of Safety and Security of Embedded Software](https://www.youtube.com/watch?v=DZSSyWlsb28)<br>
&nbsp; [Roderick Chapman & Neil White - Developing safe and secure code with SPARK](https://www.youtube.com/watch?v=VKPmBWTxW6M)<br>
&nbsp; [Shared Memory Parallelism in Ada: Load Balancing by Work Stealing](https://www.youtube.com/watch?v=ZgTH2wdEdCQ)<br>
&nbsp; [Securing the Future of Safety and Security of Embedded Software](https://www.youtube.com/watch?v=DZSSyWlsb28)<br>
&nbsp; [Roderick Chapman & Neil White - Developing safe and secure code with SPARK](https://www.youtube.com/watch?v=VKPmBWTxW6M)<br>
&nbsp; [Sequential Programming in Ada: Lessons Learned](https://archive.fosdem.org/2019/schedule/event/ada_sequential/)<br>
&nbsp; [Contract-based Programming: a Route to Finding Bugs Earlier](https://www.youtube.com/watch?v=tyA6YRlzEfU)<br>
&nbsp; [Ada, or How to Enforce Safety Rules at Compile Time](https://www.youtube.com/watch?v=PUoRZcw-3JY)<br>
&nbsp; [SPARK Language: Historical Perspective & FOSS Development](https://www.youtube.com/watch?v=Cmeub8H7uXc)<br>
&nbsp; [Making the Ada_Drivers_Library: Embedded Programming with Ada](https://www.youtube.com/watch?v=Mb4vtkA73dw)<br>
&nbsp; [SPARK 2014 - Formal Verification Made Easy](https://www.youtube.com/watch?v=plXboBx7LUs)<br>
&nbsp; [Developing Embedded Systems in Ada](https://www.youtube.com/watch?v=FTdHWjg38QE)<br>
&nbsp; [Future Airborne Capability Environment FACE Support — AdaCore Tech Days Boston 2018](https://www.youtube.com/watch?v=xzNNthX4tPA)<br>
&nbsp; [Alternative languages for safe and secure RISC-V programming](https://www.youtube.com/watch?v=y8DsNo7yfgE)<br>
***

### Rust ###
___Articles___<br>
&nbsp; [A half-hour to learn Rust](https://fasterthanli.me/articles/a-half-hour-to-learn-rust)<br>
&nbsp; [Rust explained using easy English](https://github.com/Dhghomon/easy_rust)<br>
&nbsp; [A Coding Retreat and Getting Embedded Rust Running on a SensorTag](https://www.wezm.net/technical/2019/03/sensortag-embedded-rust-coding-retreat/)<br>
&nbsp; [My First 3 Weeks of Professional Rust](https://www.wezm.net/technical/2019/03/first-3-weeks-of-professional-rust/)<br>
&nbsp; [Two Years With Rust](http://brooker.co.za/blog/2020/03/22/rust.html)<br>
&nbsp; [Rust is Surprisingly Good as a Server Language](https://stu2b50.dev/posts/rust-is-surpris76171)<br>
___Tech Talks___<br>
&nbsp; [Matthieu Wipliez - Techniques for writing concurrent applications with asynchronous I/O](https://www.youtube.com/watch?v=VNUiycotp8U)<br>
&nbsp; [Lisa Passing - Making a game in Rust](https://www.youtube.com/watch?v=Ktwl97Ph-SI&index=7) [Gamedev]<br>
&nbsp; [Unix systems programming in Rust — Kamal Marhubi](https://www.youtube.com/watch?v=Fe6_LFGiqP0)<br>
&nbsp; [Rayon: Data Parallelism for Fun and Profit — Nicholas Matsakis](https://www.youtube.com/watch?v=gof_OEv71Aw)<br>
&nbsp; [(ninth RacketCon): Aaron Turon – Governing Rust](https://www.youtube.com/watch?v=T1t4zGJYUuY)<br>
&nbsp; [Sonny Scroggin - BEAM + Rust: A match made in heaven | Code BEAM STO 19](https://www.youtube.com/watch?v=xe32ku9s14k)<br>
&nbsp; [A microkernel written in Rust: Porting the UNIX-like Redox OS to Armv8 A case study of Rust as a la…](https://www.youtube.com/watch?v=qpazyDkuqLw) [OS Design]<br>
&nbsp; [RustConf 2018 - Closing Keynote - Using Rust For Game Development by Catherine West](https://www.youtube.com/watch?v=aKLntZcp27M)<br>
&nbsp; [Idiomatic Rust Writing concise and elegant Rust code](https://www.youtube.com/watch?v=CpWGEkI7jZo)<br
&nbsp; [Considering Rust](https://www.youtube.com/watch?v=DnT-LUQgc7s)<br>
***

### C ###
&nbsp; [MISRA-C Roadmap 2017](https://www.youtube.com/watch?v=S2bigfs-Cjc)<br>
***

### C++  ###
___Tech Talks___<br>
&nbsp; [C++ Mocks Made Easy - An Introduction to gMock](https://www.youtube.com/watch?v=sYpCyLI47rM)<br>
&nbsp; [CppCon 2014: Mike Acton "Data-Oriented Design and C++"](https://www.youtube.com/watch?v=rX0ItVEVjHc)<br>
&nbsp; [CppCon 2014: Nicolas Fleury "C++ in Huge AAA Games"](https://www.youtube.com/watch?v=qYN6eduU06s)<br>
&nbsp; [CppCon 2017: Carl Cook “When a Microsecond Is an Eternity: High Performance Trading Systems in C++”](https://www.youtube.com/watch?v=NH1Tta7purM)<br>
&nbsp; [CppCon 2017: Peter Sommerlad “Mocking Frameworks considered harmful”](https://www.youtube.com/watch?v=uhuHZXTRfH4)<br>
&nbsp; [Structure and Interpretation of Computer Programs: SICP - Conor Hoekstra - CppCon 2020](https://www.youtube.com/watch?v=7oV7hiAsVTI)<br>
***

### D ###
___Articles___<br>
&nbsp; [My Vision of D’s Future](https://dlang.org/blog/2019/10/15/my-vision-of-ds-future/)<br>
&nbsp; [Metaprogramming is less fun in D](https://epi.github.io/2017/03/18/less_fun.html)<br>
___Tech Talks___<br>
&nbsp; [D as a Better C - Simon Arneaud | DConf2017 - YouTube](https://www.youtube.com/watch?v=_F5uPLdMaJg) [Youtube black holed]<br>
&nbsp; [DConf 2019: D for a @safer Linux Kernel -- Alexandru Militaru](https://www.youtube.com/watch?v=weRSwbZtKu0)<br>
***

### Forth ###
___Articles___<br>
&nbsp; [Thoughts on Forth Programming](http://www.call-with-current-continuation.org/articles/forth.txt)<br>
&nbsp; [Forth - The Early Years](https://colorforth.github.io/HOPL.html)<br>
***

### F#, OCaml, and Standard ML ###
___Articles___<br>
&nbsp; [Haskell vs OCaml](https://markkarpov.com/post/haskell-vs-ocaml.html)<br>
&nbsp; [OCaml vs Haskell](https://blog.regnat.ovh/posts/ocaml-vs-haskell/)<br>
&nbsp; [OCaml: What You Gain](http://roscidus.com/blog/blog/2014/02/13/ocaml-what-you-gain/)<br>
&nbsp; [Standard ML in 2020](https://notes.eatonphil.com/standard-ml-in-2020.html)<br>
___Tech Talks___<br>
&nbsp; [Why OCaml](https://www.youtube.com/watch?v=v1CmGbOGb2I)<br>
&nbsp; [PolyConf 16: A brief history of F# / Rachel Reese](https://www.youtube.com/watch?v=cbDjpi727aY)<br>
&nbsp; [Anthony Brown - F# for Fun and Games](https://www.youtube.com/watch?v=C3IW2fhaj5s)<br>
***

### Erlang & Elixir ###
___Articles___<br>
&nbsp; [Ten Years of Erlang](https://ferd.ca/ten-years-of-erlang.html) [Article]<br>
&nbsp; [Why am I interested in Elixir?](http://underjord.io/why-am-i-interested-in-elixir.html)<br>
&nbsp; [Erlang Tooling in 2020](https://notamonadtutorial.com/erlang-tooling-in-2020-b9606596353a)<br>
___Books___<br>
&nbsp; [Introducing Erlang, 2nd Edition](https://www.oreilly.com/library/view/introducing-erlang-2nd/9781491973363/)<br>
&nbsp; [Erlang and OTP in Action](https://www.manning.com/books/erlang-and-otp-in-action)<br>
&nbsp; [Designing for Scalability with Erlang/OTP](https://www.oreilly.com/library/view/designing-for-scalability/9781449361556/)<br>
___Tech Talks___<br>
&nbsp; [Testing Erlang and Elixir through PropEr Modeling with Fred Hebert | Erlang Solutions Webinar](https://www.youtube.com/watch?v=LvFs33-1Tbo)<br>
&nbsp; [Comparing Erlang and Go Concurrency](https://www.youtube.com/watch?v=2yiKUIDFc2I)<br>
&nbsp; [Garrett Smith - Why The Cool Kids Don't Use Erlang](https://www.youtube.com/watch?v=3MvKLOecT1I)<br>
&nbsp; [GOTO 2013 • The Erlang VM or How I Started Loving Parallel Progamming • Erik Stenman](https://www.youtube.com/watch?v=ArRr4trTCjQ)<br>
&nbsp; [An Evening at Erlang Factory: Joe Armstrong, Mike Williams, Robert Virding](https://www.youtube.com/watch?v=rYkI0_ixRDc) [History of Erlang]<br>
&nbsp; [Midwest.io 2014 - Designing a Real Time Game Engine in Erlang - Mark Allen](https://www.youtube.com/watch?v=sla-t0ZNlMU)<br>
&nbsp; [PolyConf 16: Erlang in The Land of Lisp / Jan Stepien](https://www.youtube.com/watch?v=qVMKIKJrUd8)<br>
&nbsp; [Taking Back Embedded: The Erlang Embedded Framework - Omer Kilic](https://www.youtube.com/watch?v=TUmU3XCfLLw)<br>
&nbsp; [Believe in Erlang in Games - James Mayfield, Mario Izquierdo, Noah Gift](https://www.youtube.com/watch?v=SFZV_-Ozyis) [Gamedev]<br>
&nbsp; [Robert Virding - Hitchhiker's Tour of the BEAM](https://www.youtube.com/watch?v=_Pwlvy3zz9M)<br>
&nbsp; [Surviving Fire: using Erlang as an OS to Achieve Massive Fault Tolerance by Sam Williams](https://www.youtube.com/watch?v=9XqHiumZ02E) [OS Design]<br>
&nbsp; [A Completely Unbiased Showcase of Elixir by Andrea Leopardi](https://www.youtube.com/watch?v=EPXZ6VLiZuo)<br>
&nbsp; [A reactive game stack: Using Erlang, Lua and Voltdb to enable a nonsharded game world](https://www.youtube.com/watch?v=BiBvOGP-GNg) [Gamedev]<br>
&nbsp; [Migrating an Invoicing System to Elixir/Erlang by Norberto Ortigoza](https://www.youtube.com/watch?v=ybFfv4-nLx8)<br>
&nbsp; [What I Learned Writing a Game Server in Erlang w/ QP](https://www.youtube.com/watch?v=Hf96bhU7R80) [Gamedev]<br>
&nbsp; [Intro to OTP in Elixir](https://www.youtube.com/watch?v=CJT8wPnmjTM)<br>
&nbsp; [Erlang Factory 2014 - That's 'Billion' with a 'B': Scaling to the Next Level at WhatsApp](https://www.youtube.com/watch?v=c12cYAUTXXs)<br>
&nbsp; [OTP Team - Lukas Larsson](https://www.youtube.com/watch?v=Ich_G1z44pQ)<br>
&nbsp; [OTP in Elixir - Introduction](https://www.youtube.com/watch?v=_WgrfEaAM4Y)<br>
&nbsp; [Secrets of the Erlang Beam compiler - Richard Carlsson](https://www.youtube.com/watch?v=RMKSYWz_nPo)<br>
&nbsp; [Andrea Leopardi - Papers we love - Elixir edition](https://www.youtube.com/watch?v=DuMp3362MsA)<br>
&nbsp; [Sonny Scroggin - Taking Erlang Elixir to the Metal - Code BEAM SF 2018](https://www.youtube.com/watch?v=F86JXaCPowo)<br>
&nbsp; [Duncan Sparrell - Let it Be Hacked - Code Beam SF 2018](https://www.youtube.com/watch?v=NYkwvVKlbU8)<br>
&nbsp; [Fred Hebert - Operable Erlang and Elixir | Code BEAM SF 19](https://www.youtube.com/watch?v=OR2Gc6_Le2U)<br>
&nbsp; [GOTO 2019 • The Soul of Erlang and Elixir • Saša Jurić](https://www.youtube.com/watch?v=JvBT4XBdoUE)<br>
&nbsp; [Erlang Factory 2014 -- Eliminating Single Process Bottlenecks with ETS Concurrency Patterns](https://www.youtube.com/watch?v=XrkY9WRY8p0)<br>
&nbsp; [Neuroevolution Through Erlang - Gene Sher](https://www.youtube.com/watch?v=TcUqGIHq8rA)<br>
&nbsp; [Wireless Small Embedded Erlang Applications (...) - Peer Stritzinger (Lambda Days 2017)](https://www.youtube.com/watch?v=W0P-l7dBGJk)<br>
&nbsp; [Erlang: the Power of Functional Programming - Simon Thompson - EFLBA2017](https://www.youtube.com/watch?v=pJwfyPV_V_o)<br>
&nbsp; [Erik Stenman - BEAM: What Makes Erlang BEAM - Code Mesh 2017](https://www.youtube.com/watch?v=FonRzASOkZE)<br>
&nbsp; [José Valim - Idioms for building distributed fault-tolerant applications with Elixir](https://www.youtube.com/watch?v=MMfYXEH9KsY)<br>
&nbsp; [Erlang Express Course](https://www.youtube.com/watch?v=aEyQcZg-Njs) [Lecture Series]<br>
***

### Julia ###
___Articles___<br>
&nbsp; [Meeting Julia, a great new alternative for numerical programming — Part I benchmarking](https://medium.com/@nwerneck/meeting-julia-a-great-new-alternative-for-numerical-programming-part-i-benchmarking-c03dd3289493)<br>
&nbsp; [Meeting Julia, a great new alternative for numerical programming — Part II a high-level perspective](https://medium.com/@nwerneck/meeting-julia-a-great-new-alternative-for-numerical-programming-part-ii-a-high-level-perspective-170df0eee6c0)<br>
&nbsp; [JuliaCon2020: Julia is production ready!](https://bkamins.github.io/julialang/2020/08/07/production-ready.html)<br>
___Tech Talks___<br>
&nbsp; [JuliaCon 2018 | Teaching Statistics to the Masses with Julia](https://www.youtube.com/watch?v=-_PcJGdpwb4)<br>
***

### Lisps ###
___Articles___<br>
&nbsp; [Why i chose PicoLisp as main language for my projects](https://www.mail-archive.com/picolisp@software-lab.de/msg09621.html)<br>
___Tech Talks___<br>
&nbsp; [Robert Virding - LFE - a lisp flavour on the Erlang VM (Lambda Days 2016)](https://www.youtube.com/watch?v=Br2KY12LB2w)<br>
&nbsp; [LFE: A Real Lisp in the Erlang Ecosystem by Robert Virding](https://www.youtube.com/watch?v=x2ysisqgd2g)<br>
&nbsp; [Veit Heller - Carp - A Language for the 21st Century](https://www.youtube.com/watch?v=Q1BVfGIhwZI)<br>

#### Common Lisp ####
___Articles___<br>
&nbsp; [A Road to Common Lisp](http://stevelosh.com/blog/2018/08/a-road-to-common-lisp/)<br>
&nbsp; [Why I haven't jumped ship from Common Lisp to Racket (just yet)](https://fare.livejournal.com/188429.html)<br>
___Tech Talks___<br>
&nbsp; [The Structure Is the Data, by John Lamping (lightning talk)](https://www.youtube.com/watch?v=wPRqjQAVym0)<br>
&nbsp; [Visualizing Data Structures, by Arthur A. Gleckler (lightning talk)](https://www.youtube.com/watch?v=SMO69X3QH0k)<br>

#### Clojure ####
___Articles___<br>
&nbsp; [What I learned after writing Clojure for 424 days, straight](https://medium.com/swlh/what-i-learned-after-writing-clojure-for-424-days-straight-8884ec471f8e)<br>
&nbsp; [Why I Switched from Python to Clojure](https://www.bradcypert.com/why-i-switched-from-python-to-clojure/)<br>
&nbsp; [Guide to starting with Clojure](https://grison.me/2020/04/04/starting-with-clojure/)<br>
___Tech Talks___<br>
&nbsp; [clojureD 2019: "Our Journey from Elm and Elixir to Clojure" by Martin Kavalar](https://www.youtube.com/watch?v=geeK1-jjlhY)<br>
&nbsp; [Solving Problems the Clojure Way - Rafal Dittwald](https://www.youtube.com/watch?v=vK1DazRK_a0)<br>
&nbsp; [Lessons Learned Creating a Successful Machine Learning Startup with Clojure - Alex Hudek](https://www.youtube.com/watch?v=u-4FiFpkPlQ)<br>
&nbsp; [Fun(c) 2018.5: Derek Slager - a Functional Startup with Clojure](https://www.youtube.com/watch?v=kQOCTG8mMvI)<br>

#### Racket ####
___Articles___<br>
&nbsp; [Racket is an acceptable Python](https://dustycloud.org/blog/racket-is-an-acceptable-python/)<br>
&nbsp; [Implementing MsgPack.rkt, part 1](https://hiphish.github.io/blog/2019/02/11/implementing-msgpack.rkt-pt1/)<br>
&nbsp; [Implementing MsgPack.rkt, part 2](https://hiphish.github.io/blog/2019/02/13/implementing-msgpack.rkt-pt2/)<br>
&nbsp; [Implementing MsgPack.rkt, part 3](https://hiphish.github.io/blog/2019/02/15/implementing-msgpack.rkt-pt3/)<br>
___Tech Talks___<br>
&nbsp; [(fourth RacketCon): Jay McCarthy — Get Bonus! Infinite Functional Entertainment at 60 FPS!](https://www.youtube.com/watch?v=_x0Ob2HY8C4)<br>
&nbsp; [(seventh RacketCon): William G Hatch -- Rash: Reckless Shell Programming in Racket](https://www.youtube.com/watch?v=yXcwK3XNU3Y)<br>
&nbsp; [(sixth RacketCon): Alexis King -- Languages in an Afternoon](https://www.youtube.com/watch?v=TfehOLha-18)<br>
&nbsp; [(fourth RacketCon): Neil Toronto — Purely Functional 3D in Typed Racket](https://www.youtube.com/watch?v=t3xdv4UP9-U)<br>
&nbsp; [(sixth RacketCon): Matthew Butterick -- The Making of “Beautiful Racket”](https://www.youtube.com/watch?v=o_ptYjf8Bes)<br>
&nbsp; [Rebuilding Racket on Chez Scheme: An Experience Report](https://www.youtube.com/watch?v=t09AJUK6IiM)<br>
&nbsp; [A Guiler's Year of Racket](https://www.youtube.com/watch?v=R-hy8xLlkHA)<br>
&nbsp; [(fifth RacketCon): Matthias Felleisen — The Racket Manifesto](https://www.youtube.com/watch?v=JnczIyPXGfc)<br>
&nbsp; [(fourth RacketCon): David Vanderson — Racket for a networked multiplayer game](https://www.youtube.com/watch?v=Fuz0BtltU1g)<br>
&nbsp; [(seventh RacketCon): Jay McCarthy -- Teaching and Designing Microarchitecture in Racket](https://www.youtube.com/watch?v=6u1dFGlt5MA)<br>
&nbsp; [(seventh RacketCon): Deren Dohoda: Prototype to Production -- A Racket Experience Report](https://www.youtube.com/watch?v=smt8piP-_gk)<br>
&nbsp; [(ninth RacketCon): Greg Hendershott – Racket and Emacs: Fight!](https://www.youtube.com/watch?v=NDHi6aWyI0Y)<br>

#### Scheme ####
___Articles___<br>
&nbsp; [The evolution of a Scheme programmer](https://erkin.party/blog/200715/evolution/)<br>
___Tech Talks___<br>
&nbsp; [PolyConf 16 / Knit, Chisel, Hack: Building Programs in Guile Scheme / Andy Wingo](https://www.youtube.com/watch?v=TVO8WXFYDIA)<br>
&nbsp; [Guile 3: Faster programs via just-in-time compilation](https://www.youtube.com/watch?v=fWv4AlVbJZ8)<br>
&nbsp; [Lightning Talk: Gerbil on Gambit, as they say Racket on Chez](https://www.youtube.com/watch?v=C3rzbs_8gNc)<br>
***

### Elm ###
___Articles___<br>
&nbsp; [Re-writing Vy.no in Elm](https://blogg.bekk.no/using-elm-at-vy-e028b11179eb)<br>
&nbsp; [Why I’m leaving Elm](https://lukeplant.me.uk/blog/posts/why-im-leaving-elm/)<br>
&nbsp; [The Biggest Problem with Elm](https://medium.com/@cscalfani/the-biggest-problem-with-elm-4faecaa58b77)<br>
___Tech Talks___<br>
&nbsp; [Erlang Factory SF 2016 Keynote Phoenix and Elm – Making the Web Functional](https://www.youtube.com/watch?v=XJ9ckqCMiKk)<br>
&nbsp; [Tomasz Kowal - Elixir and Elm - the perfect couple (Lambda Days 2016)](https://www.youtube.com/watch?v=mIwD27qqr5U)<br>

### Others ###
___Articles___<br>
&nbsp; [APL – a Glimpse of Heaven](http://archive.vector.org.uk/art10011550) [APL]<br>
___Tech Talks___<br>
&nbsp; [OSCON 2015 - Andreas Rumpf - Nim: An Overview](https://www.youtube.com/watch?v=4rJEBs_Nnaw) [Nim]<br>
***
